#!/bin/sh
apt-get update
apt-get -y install nginx nginx-extras
service nginx stop
mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
cp ./nginx.conf /etc/nginx/nginx.conf
cp -r ./VeryNginx /etc/nginx/VeryNginx
service nginx start
