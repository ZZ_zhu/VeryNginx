#!/bin/sh
apt-get install -y software-properties-common
apt-get install -y python-software-properties
apt-add-repository ppa:brightbox/ruby-ng-experimental
apt-get update
apt-get install -y ruby2.2 ruby2.2-dev
apt-get install -y sqlite3
apt-get install libcurl4-openssl-dev
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7
apt-get install -y build-essential git-core bison openssl libreadline6-dev curl zlib1g zlib1g-dev libssl-dev libyaml-dev libsqlite3-0 libsqlite3-dev sqlite3  autoconf libc6-dev libpcre3-dev curl libcurl4-nss-dev libxml2-dev libxslt-dev imagemagick nodejs libffi-dev
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7
apt-get install -y apt-transport-https ca-certificates
sh -c 'echo deb https://oss-binaries.phusionpassenger.com/apt/passenger trusty main > /etc/apt/sources.list.d/passenger.list'
apt-get update
apt-get install -y nginx-extras passenger
apt-get install -y nginx-extras
service nginx stop
mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
cp ./nginx.passenger.conf /etc/nginx/nginx.conf
cp -r ./VeryNginx /etc/nginx/VeryNginx
service nginx start
